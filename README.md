# devops

#### 介绍
该仓库记录本人在CSDN上发布的 devops 体系中，docker-compose 脚本、映射目录 及 nginx域名代理信息等,CSDN专栏地址 https://blog.csdn.net/tysite/category_9466543.html

使用本项目前，请参照专栏前四章搭建虚拟环境

#### 目录规划
devops体系采用centOS 7.4 作为操作系统，根目录为 `/devops`,`docker-compose.yml`文件存放在此目录中

#### 目录创建脚本
- 请按照顺序执行目录创建脚本

````
sudo mkdir -p /devops/registry/data
sudo mkdir -p /devops/gitlab/data /devops/gitlab/log /devops/gitlab/conf
sudo mkdir -p /devops/nexus/data
chmod 777 /devops/nexus/data
````

#### devops 域名规划
|ip地址 | 域名 | 端口 | 说明 |
|--|--|--|--|
| 192.168.43.150 | www.tysite.org | 80 | nginx web服务器，未来作为项目主页使用 |
| 192.168.43.150 | git.tysite.org | 10080 | git服务器访问地址 |
| 192.168.43.150 | nexus.tysite.org | 18081 | nexus服务器管理地址 |

注意：nginx 的配置文件，请从 `conf.d` 文件夹中下载

#### 本地 hosts 配置
````
192.168.43.150 www.tysite.org
192.168.43.150 git.tysite.org
192.168.43.150 nexus.tysite.org
````